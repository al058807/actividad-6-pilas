*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilas;
import java.util.Stack;
import java.util.Scanner;


/**
 *
 * @author loli_
 */
public class Ejercicio3pila {
  public static void main(String[] args) {
   System.out.println("Cinthia Guadalupe Trejo Moo");
System.out.println("Matricula:58807");
System.out.println("Materia:Estructura de Datos");  
        String cadena="";
        String A="";
        String E="";
        String I="";
        String O="";
        String U="";
        String f = "S";

        while (f.equalsIgnoreCase("S")){
            System.out.println ("Escriba la palabra u oracion que desea analizar");
            Scanner entrada = new Scanner(System.in);
            cadena=entrada.nextLine();
            
            switch (analizarA(cadena)){
                case 1: 
                    A="par"; 
                break;
                case -1:
                    A="impar"; 
                break;
                case 0: 
                    A= "cero";
                break;
            }
            switch (analizarE(cadena)){
                case 1:
                    E="par"; 
                break;
                case -1: 
                    E="impar"; 
                break;
                case 0: 
                    E= "cero"; 
                break;
            }
            switch (analizarI(cadena)){
                case 1: 
                    I="par"; 
                break;
                case -1: 
                    I="impar";
                break;
                case 0: 
                    I= "cero"; 
                break;
            }
            switch (analizarO(cadena)){
                case 1: 
                    O="par"; 
                break;
                case -1:
                    O="impar";
                break;
                case 0:
                    O= "cero";
                break;
            }
            switch (analizarU(cadena)){
                case 1:
                    U="par"; 
                break;
                case -1: 
                    U="impar"; 
                break;
                case 0: 
                    U= "cero"; 
                break;
            }
            System.out.println ("----Datos---- "
                    + "\nEl número de letras A es " +A
                    + "\nEl número de letras E es " +E
                    + "\nEl número de letras I es " +I
                    + "\nEl número de letras O es " +O
                    + "\nEl número de letras U es " +U);
            
            System.out.print ("---¿Quiere analizar otra palabra u oracion?--- "
                    + "\nS.-si"
                    + "\nN.-no\n");
            f = entrada.nextLine();
        }
    }
    //Contar a
    public static int analizarA (String cadena) {
        Stack<String> pila = new Stack<String>();
        int i=0; 
        int x=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='a'&&pila.empty()) {
                pila.push("a"); 
                x++;
            }
            else if (cadena.charAt(i)=='a'&&!pila.empty()) {
                pila.pop();
            }
            i++;
        }
        if (x==0) {
            return 0;
        } else {
            if (pila.empty()) {
                return 1;
            } else {
                return -1;
            }
        }     
    }
 //Contar e
    public static int analizarE (String cadena) {
        Stack<String> pila = new Stack<String>(); 
        int i=0; 
        int y=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='e'&&pila.empty()) {
                pila.push("e"); 
                y++;
            }
            else if (cadena.charAt(i)=='e'&&!pila.empty()) {
                pila.pop();
            }
            i++;
        }
        if (y==0) {
            return 0;
        } else {
            if (pila.empty()) {
                return 1;
            } else {
                return -1;
            }
        }
    }
     //Contar i
    public static int analizarI (String cadena) {
        Stack<String> pila = new Stack<String>(); 
        int i=0; 
        int z=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='i'&&pila.empty()) {
                pila.push("i");
                z++;
            }
            else if (cadena.charAt(i)=='i'&&!pila.empty()) {
                pila.pop();
            }
            i++;
        }       
        if (z==0) {
            return 0;
        } else { 
            if (pila.empty()) {
                return 1;
            } else {
                return -1;
            }
        }
    }
     //Contar o
    public static int analizarO (String cadena) {
        Stack<String> pila = new Stack<String>(); 
        int i=0; 
        int t=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='o'&&pila.empty()) {
                pila.push("o"); 
                t++;
            }
            else if (cadena.charAt(i)=='o'&&!pila.empty()) {
                pila.pop();
            }
            i++;
        }
        if (t==0) {
            return 0;
        } else { 
            if (pila.empty()) {
            return 1;
        } else {
            return -1;}
        }     
    }
     //Contar u
    public static int analizarU (String cadena) {
        Stack<String> pila = new Stack<String>(); 
        int i=0; 
        int r=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='u'&&pila.empty()) {
                pila.push("u");
                r++;
            }
            else if (cadena.charAt(i)=='u'&&!pila.empty()) {
                pila.pop();
            }
            i++;
        }
        if (r==0) {
            return 0;
        } else {
            if (pila.empty()) {
                return 1;
            } else {
                return -1;
            }
        }
    }
    

   
}
