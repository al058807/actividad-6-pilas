* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilas;
import java.util.*;
/**
 *
 * @author loli_
 */
public class Ejercicio4pila {
   
    static Stack<Object> stack = new Stack<Object>();

    private Pilas ultimodatoingresado;
    int tamaño;
    int r;
    String lista=" ";
    
    public Ejercicio4pila(){
        ultimodatoingresado=null;
        tamaño=0;
    }
    
    public int TamanoPila(){
        return tamaño;
    }
    
 
    public void ponerdato(int Dato){
        Pilas nuevo=new Pilas(Dato);
        nuevo.siguiente=ultimodatoingresado;
        ultimodatoingresado=nuevo;
        tamaño++; 
    }
  
     public int ultimodato(){
        return ultimodatoingresado.informacion;
    }
  
    public int eliminardato(){
        int cancelar=ultimodatoingresado.informacion;
        ultimodatoingresado=ultimodatoingresado.siguiente;
        tamaño--;
        return cancelar;
    }
    
    public boolean PilaVacia(){
        return ultimodatoingresado == null;
    }
    
    public void mostrardatos(){
        Pilas recorrido = ultimodatoingresado;
        
        while(recorrido != null){
            lista += recorrido.informacion + "\n";
            recorrido = recorrido.siguiente;
            
        }
        System.out.println("Contenido de la Pila"+"\n"
                +lista);
        
    }    
   
    
    public static void main(String[] args) {
      int opcion = 0, n = 0;
        Ejercicio4pila pila = new Ejercicio4pila();
        
        do{
            try{
                Scanner leer= new Scanner(System.in);
                System.out.println("=====opiones========\n"
     + "1.-Insertar un dato en la pila\n"
     + "2.-sumar dato\n"
     + "3.-restar dato\n"
     + "4.-cancelar el ulitmo dato ingresado\n"
     + "5.-ver la pila\n"
     + "6.-Terminar\n"
     + "================");
                
                System.out.println("Elige un numero del 0 al 7");
                opcion=leer.nextInt();
                
                switch (opcion) {
                    case 1:
                        System.out.println("ingrese para guardar en la pila");
                        n=leer.nextInt();
                        pila.ponerdato(n);
                        break;
                    
                    case 2:
                        int x;
                        int y;
                        if(!pila.PilaVacia()){
                            x=pila.ultimodato();
                            
                            System.out.println("ultimo dato + ");
                                    n=leer.nextInt();
                            
                                y= x + n;
                                
                                System.out.println("= "+ y);
                            pila.ponerdato(y);
                        }
                        else {
                            System.out.println("La pila esta vacia");
                        }
                        break;
                        
                    case 3: 
                        int o;
                        int p;
                        if(!pila.PilaVacia()){
                            o=pila.ultimodato();
                            
                            System.out.println("ultimo dato - ");
                                    n=leer.nextInt();
                            
                                p= o - n;
                                
                                System.out.println("= "+p);
                            pila.ponerdato(p);
                        }
                        else {
                            System.out.println("La pila esta vacia");
                        }
                        break;   
                    
                    case 4:
                        if(!pila.PilaVacia()){
                            System.out.println("Se ha eliminado el dato en la pila" + pila.eliminardato());
                        }else {
                            System.out.println("La pila esta vacia");
                        }
                        break;
                    
                    case 5:
                        pila.mostrardatos();
                        break;
                    
                    case 6:
                        opcion = 6;
                        break;
                        
                    default:
                        System.out.println("Pila terminada");
                        break;     
                }
            }catch (NumberFormatException e) {
                
            }
        }while (opcion != 6);
    }

    


}
